## Coisas pra Fazer em Estocolmo

### Kaknästornet
É uma torre super alta de onde se tem uma ótima visão de Estocolmo (espcialmente de Djurgården). Lá em cima tem um café e um restaurante. É um passeio curto, mas bem interessante.

Primavera/Verão: * * *

Outono/Inverno: * * *

### Stadhuset
A Prefeitura de Estocolmo. É muito bonita e fica numa região bem bonita também. O passeio em si é andar pela parte interna do prédio e visitar o jardim que fica a beira mar. Tudo ao ar livre.

Primavera/Verão: * * *

Outono/Inverno: * * *

### Hagaparken
Um dos maiores parques da cidade, onde inclusive fica um dos palácios, onde vive a princesa Victoria. O parque é muito bonito no verão ou no inverno. Pra entrar no parque não paga nada. Tem um borboletário + aquário no parque, mas acho muito caro para o que eles tem lá.

Primavera/Verão: * * * *

Outono/Inverno: *

### Skansen
É uma espécie de parque + museu a céu aberto + zoológico. Tem um restaurante muito bom e é uma delícia passear por todas essas atrações tanto no verão quanto no inverno.

Primavera/Verão: * * * * *

Outono/Inverno: * * * *

### Vasa Museet
Um museu construído ao redor de um galeão real sueco que afundou no século XVII logo depois de ser lançado ao mar. É um dos museus mais impressionantes que já visitei. Esse é visita obrigatória.

Primavera/Verão: * * * * *

Outono/Inverno: * * * * *

### Nordiska Museet
Um museu gigantesco sobre a cultura nórdica. Vale muito a pena pra conhecer mais sobre os costumes da escandinávia no passado e no presente

Primavera/Verão: * * * *

Outono/Inverno: * * * *

### Tekniska Museet
Um museu de tecnologia, com um monte de atividades interativas. É agradável para adultos, mas especialmente voltado para as crianças se divertirem.

Primavera/Verão: * * *

Outono/Inverno: * * *

### IKEA
Uma das maiores lojas da Ikea no mundo, é um lugar pra conhecer e/ou comprar. Como passeio é mais curioso do que interessante

Primavera/Verão: * *

Outono/Inverno: * *

### Uppsala
Cidade ao norte de Estocolmo que dá pra ir de trem em uma hora. A cidade em si é uma cidade universitária com museus, uma igreja e outros lugares interessantes para passear. No inverno é um pouco manos atratova pois a maioria dos lugares pra ir é ao ar livre e se vai a pé

Primavera/Verão: * * *

Outono/Inverno: * *

### Uppsala ### Fazenda de Morango
Um pouco afastado de Uppsala há uma fazenda onde durante a primavera e o verão é possível ir e pagar um preço fixo pra colher quanto morango for possível e trazer com vc.

Primavera/Verão: * * *

### Kungliga Slottet
O palácio real no centro da cidade. Não é um passeio longo, mas é bem interessante. Tem a sala do trono, a capela e outras salas. Embaixo do palácio tem o museu da cavalaria e ao lado tem o museu medieval, ambos bem interessantes também

Primavera/Verão: * * *

Outono/Inverno: * * *

### Gustavberghamn (e Delselius)
É um pequeno porto no lado leste da cidade, já indo pros lado do arquipélago. Tem uma vista muito bonita e uma padaria/doceria sensacional chamada Delselius

Primavera/Verão: * * *

Outono/Inverno: * *

### Drottningholm
O palácio da rainha. O palácio é super bonito e há também um passeio pra fazer num teatro que fica dentro do palácio e que é do tempo em que o teatro floreceu na Suécia.

Primavera/Verão: * * * *

Outono/Inverno: * *	*

### Gripsholm Slott
Um castelo bem legal, mais afastado de Estocolmo e que mostra muito do estilo de vida da corte sueco entre os séculos XV e XVIII. Cheio de mobiliário de época e muitos muitos quadros

Primavera/Verão: * * *

Outono/Inverno: * *	*

### Globen
Uma arena de eventos que possui um elevador panorâmico pra ver uma parte da cidade de cima.

Primavera/Verão: * *

Outono/Inverno: * *

### Hellasgården (Sauna, lago, patinação)
Um lago onde é possível patinar no inverno e fazer caminhadas longas no verão. Além do lago (onde é possível nadar no verão e no inverno) há um restaurante, um pequeno café e uma sauna (que combina bem com o lago)

Primavera/Verão: * * * *

Outono/Inverno: * * * *

### Akkurat / Omnipollo hat
Dois pubs em Södermalm (o bairro da balada) que são muito bons. Akkurat é um bar incrível com cervejas do mundo todo e opções de comida (entre ela o ótimo balde com 1kg de mexilhões). Omnipollo é o bar de uma cervejaria sueca com cervejas locais muito saborosas e que também faz uma ótima pizza

Primavera/Verão: * * * *

Outono/Inverno: * * * *

### Gondolen
O restaurante é elegante e sofisticado. Contudo, mesmo sem ir ao restaurante é possível subir no mirante logo acima de onde se tem uma vista lindíssima da cidade. No verão o mirante se torna um terraço com bar e sofazinhos pra curtir os longos fins de tarde

Primavera/Verão: * * *

Outono/Inverno: * * *

### Gamla Stan
Literalmente "a cidade velha". O centro velho de Estocolmo por onde se pode andar por ruas estreitas e visitar lugares curiosos e bonitos como a Catedral da Cidade, a igreja alemã, o museu do Nobel, a praça central da cidade e outros lugares. Há passeios a pé gratuitos saindo da estaçaão de metrô

Primavera/Verão: * * * * *

Outono/Inverno: * * * *

### The Hairy Pig (pub in Gamla Stan)
Um pequeníssimo pub na cidade velha, mas que serve boa cerveja e petiscos. Um bom lugar pra se esconder do frio

Primavera/Verão: * * * *

Outono/Inverno: * * *

### Aifur (restaurante viking)
Restaurante temático viking. A comida é muito boa, apesar de um pouco cara, e a decoração e o atendente que apresenta cada novo visitante que chega são atrações a parte

Primavera/Verão: * * * *

Outono/Inverno: * * * *

### Riddarholmen Kyrkan
Uma igreja muito antiga e que se tornou o local do repouso final de nobres suecos. Possui mausoléus muito interessantes além de brasões e homenagens a quase todas as famílias nobres dos países nórdicos. Abre apenas na primavera/verão

Primavera/Verão: * * * * *

### Orla de Södermalm
A orla de uma das ilhas mais legais da cidade é um ótimo passeio pra conhecer praias suecas, bares a beira mar e hotéis-barco. É um longo passeio a pé ao redor de metade da ilha e bem mais agradável de fazer na primavera ou verão

Primavera/Verão: * * *

Outono/Inverno: *		

### Vaxholm
Um forte e museu construídos numa ilha ao lado de Vaxholm são as principais atrações dessa pequena ilha. A vista é linda e no verão é um ótimo lugar pra fazer um pique-nique

Primavera/Verão: * * * * *

Outono/Inverno: * * *

### Gröna Lund
O parque de diversões da cidade com brinquedos como montanhas-russas e torres. O parque também recebe shows de bandas e cantores(as) famosas. Abre de Maio a Setembro.

Primavera/Verão: * * * * *

### Sandhamn
Uma ilha no arquipélago de Estocolmo com vistas sensacionais, praias e casas de veraneio. É possível visitar o ano todo, mas é muito mais agradável no verão

Primavera/Verão: * * * *

Outono/Inverno: * *

### Morfar Ginko (Mariatorget)
### LAIKA (Hornstull)
Bares com DJs tocando música eletrônica
